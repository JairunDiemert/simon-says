const gameSequence = [];
let playerSequence = [];
let level = 0;

const buttons = {
    green: document.getElementById('green'),
    red: document.getElementById('red'),
    yellow: document.getElementById('yellow'),
    blue: document.getElementById('blue')
};

document.getElementById('start').addEventListener('click', startGame);

function startGame() {
    level = 0;
    gameSequence.length = 0;
    playerSequence.length = 0;
    document.getElementById('message').textContent = '';
    // Re-enable game buttons
    Object.values(buttons).forEach(button => button.style.pointerEvents = '');
    nextLevel();
}

function nextLevel() {
    level++;
    playerSequence.length = 0;
    gameSequence.push(Object.keys(buttons)[Math.floor(Math.random() * 4)]);
    showSequence();
}

function showSequence() {
    let i = 0;
    const moves = setInterval(() => {
        activateButton(gameSequence[i]);
        i++;
        if (i >= gameSequence.length) {
            clearInterval(moves);
        }
    }, 600);
}

function activateButton(color) {
    buttons[color].classList.add('active');
    setTimeout(() => buttons[color].classList.remove('active'), 300);
}

Object.values(buttons).forEach(button => {
    button.addEventListener('click', (event) => {
        const color = event.target.id;
        playerSequence.push(color);
        checkPlayerMove(color);
        if (playerSequence.length === gameSequence.length && playerSequence.every((val, index) => val === gameSequence[index])) {
            setTimeout(nextLevel, 1000);
        }
    });
});

function checkPlayerMove(color) {
    if (playerSequence[playerSequence.length - 1] !== gameSequence[playerSequence.length - 1]) {
        document.getElementById('message').innerHTML = `Thanks for Playing! <br> Score: ${level}`;
        Object.values(buttons).forEach(button => button.style.pointerEvents = 'none');
    } else {
        activateButton(color);
    }
}


