# Simon Says Game

A simple implementation of the classic Simon Says game using HTML, CSS, and JavaScript.

## Description

This project is a web-based version of the Simon Says game. Players must follow a sequence of lights and sounds, repeating the sequence correctly to progress to the next level.

## File Structure

```
.
├── README.md
├── css
│   └── styles.css
├── index.html
└── js
    └── script.js
```

## How to Play

- Open `index.html` in a web browser.
- Click the "Start" button to begin the game.
- Follow the sequence of lights displayed.
- Click on the colored buttons in the same order as the sequence.
- The game progresses with longer sequences at each level.
- If you press the wrong button, the game will end.

## Hosting on GitLab Pages

This project is configured to be easily hosted on GitLab Pages using the provided `.gitlab-ci.yml` file.

## Contributions

Feel free to fork this project, submit issues, and open pull requests to contribute improvements or enhancements.

Enjoy the game!